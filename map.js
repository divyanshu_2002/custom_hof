function customMap(array, callBack) {
    const mapArray = [];
    if (typeof callBack == "function" && Array.isArray(array)) {
        for (let index = 0; index < array.length; index++) {
            mapArray.push(callBack(array, index));
        }
    }

    return mapArray;
}

module.exports = customMap;