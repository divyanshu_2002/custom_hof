function customReduce(array,callBack,startingValue){
    let accumulator;
    let startingIndex;

    if(Array.isArray(array) && typeof callBack=="function"){
        if(startingValue==undefined && array.length>0){
            accumulator=array[0];
            startingIndex=1;
        }
        else if(startingValue!=undefined){
            accumulator=startingValue;
            startingIndex=0;
        }

        for(let index=startingIndex;index<array.length;index++){
            accumulator=callBack(accumulator,array[index]);
        }

        return accumulator;
    }
    else{
        return undefined;
    }

}

module.exports=customReduce;