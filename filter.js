function customFilter(array, callBack) {
    if (Array.isArray(array) && typeof callBack == "function") {
        const filteredArray = [];
        for (let index = 0; index < array.length; index++) {
            if (callBack(index, array) == true) {
                filteredArray.push(array[index]);
            }
        }
        return filteredArray;
    }
    else {
        return [];
    }
}

module.exports = customFilter;