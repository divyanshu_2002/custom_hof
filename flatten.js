function customFlatten(array) {

    let flattenedArray = [];

    if (Array.isArray(array)) {

        for (let index = 0; index < array.length; index++) {

            if (Array.isArray(array[index])) {

                flattenedArray = flattenedArray.concat(customFlatten(array[index]));
            }
            else {

                flattenedArray.push(array[index]);
            }

        }
        return flattenedArray;
    }
    else {
        return [];
    }
}

module.exports = customFlatten;