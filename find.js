function customFind(array, callBack) {

    if (Array.isArray(array) && typeof callBack == "function") {
        for (let index = 0; index < array.length; index++) {
            if (callBack(array, index)) {
                return array[index];
            }
        }
    }
    else {
        return undefined;
    }

}


module.exports = customFind;

