const array = [1, 2, 3, 4, 5, 5];

const customForEach = require("../each");

try {
    customForEach(array, (array, index) => {
        console.log(`Element at index ${index} is ${array[index]}`);
    });

}
catch (err) {
    console.log(err.message);
}


