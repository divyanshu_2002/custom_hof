const array = [1, 2, 3, 4, 5, 5];

const customFilter = require("../filter");

try {
    const filteredArray = customFilter(array, (index, array) => {
        if (array[index] % 2 == 0) {
            return true;
        }
        else {
            return false;
        }
    });

    if (customFilter) {
        console.log(`The filtered array of even numbers is  ${filteredArray}`);
    }
    else {
        console.log("No element satisfied the condition");
    }
}
catch (error) {
    console.log(error.message);
}