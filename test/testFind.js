const array = [1, 2, 3, 4, 5, 5];

const customFind = require("../find");

try {
    const findedElement = customFind(array, (array, index) => {
        if (array[index] > 4) {
            return true;
        }
    })

    if (findedElement == undefined) {
        console.log(`No element matched with the specified condition`);
    }
    else {
        console.log(`The element that is finded by the specified condition is ${findedElement}`);
    }
}
catch (error) {
    console.log(error.message);
}