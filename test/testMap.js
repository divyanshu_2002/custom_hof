const array = [1, 2, 3, 4, 5, 5];

const customMap = require("../map");

try {
    let newMappedArray = customMap(array, (array, index) => {
        return array[index] * 2;
    });

    if (newMappedArray) {
        console.log(`The returned array from custom map function is ${newMappedArray} `);
    }
    else {
        console.log("Passed array in custom map function should be at least off size 1.")
    }
}
catch (error) {
    console.log(error.message);
}
