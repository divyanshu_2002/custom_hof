const customReduce = require("../reduce");

const array = [1, 2, 3, 4, 5, 5];

try {
    const reducedResult = customReduce(array, (accumulator, element) => {
        return accumulator + element;
    }, 5);

    if (reducedResult != undefined) {
        console.log(`The reduced result of the custom reduce function is ${reducedResult}`);
    }
    else {
        console.log("We have to pass an array and callBack function in our custom reduce functions.");
    }

}
catch (error) {
    console.log(error.message);
}

