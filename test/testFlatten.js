const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

const customFlatten=require("../flatten");

try{
    const flattenedArray=customFlatten(nestedArray);
    if(flattenedArray){
        console.log(`The flattened array is `+flattenedArray);
    }
    else{
        console.log(`Pass the array with more than size 0 to flatten.`);
    }
}
catch(error){
    console.log(error.message);
}