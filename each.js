function customForEach(array, callBack) {
    if (typeof callBack == "function" && Array.isArray(array)) {
        for (let index = 0; index < array.length; index++) {
            callBack(array, index);
        }
    }
    else {
        console.log("Please ensure that in customEach array and callBack method is passed");
    }

}

module.exports = customForEach;